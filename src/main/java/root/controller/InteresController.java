/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.model.CalcularInteres;

/**
 *
 * @author Alonso
 */
@WebServlet(name = "InteresController", urlPatterns = {"/InteresController"})
public class InteresController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet InteresController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet InteresController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("capital:"+ request.getParameter("capital"));
        System.out.println("tasa:"+ request.getParameter("tasa")); 
        System.out.println("años:"+ request.getParameter("anos")); 
        
        int Capital= Integer.parseInt(request.getParameter("capital")) ;
        int Tasa= Integer.parseInt(request.getParameter("tasa"));
        int A= Integer.parseInt(request.getParameter("a"));
        
        System.out.println("");
        CalcularInteres interes= new CalcularInteres();
        interes.calculoInteres(Capital, Tasa, A);
        request.setAttribute("capital", Capital);
        request.setAttribute("Tasa", Tasa);
        request.setAttribute("Años", A);
        request.setAttribute("interes", interes);
        request.getRequestDispatcher("interes-total.jsp").forward(request, response);
        
        
        
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
