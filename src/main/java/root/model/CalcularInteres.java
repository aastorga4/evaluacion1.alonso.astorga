/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model;

/**
 *
 * @author Alonso
 */
public class CalcularInteres {
  private float c, i,  n, total;
    
    public float getTotal(){
       return total; 
    }
    
     public void setTotal(float total){
       this.total= total; 
    }
    /**
     * @return the c
     */
    public float getC() {
        return c;
    }

    /**
     * @param c the c to set
     */
    public void setC(float c) {
        this.c = c;
    }

    /**
     * @return the i
     */
    public float getI() {
        return i;
    }

    /**
     * @param i the i to set
     */
    public void setI(float i) {
        this.i = i;
    }

    /**
     * @return the n
     */
    public float getN() {
        return n;
    }

    /**
     * @param n the n to set
     */
    public void setN(float n) {
        this.n = n;
    }
    public void calculoInteres(float c,float i,float n){
        this.total=(c*n)* (i/100);
    
    }
    
}

